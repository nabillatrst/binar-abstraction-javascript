class Sepatu {
    constructor(jenisSepatu,warna,ukuran,brand){
        this.jenisSepatu = jenisSepatu;
        this.warna = warna;
        this.ukuran = ukuran;
        this.brand = brand;
    }

    whatMyKicksToday(){
        return `sepatu ${this.jenisSepatu} berwarna ${this.warna} berukuran ${this.ukuran} ini bermerk ${this.brand}`;
    }
}

let Nike = new Sepatu("sporty","white","43","Nike");
console.log(Nike.whatMyKicksToday());