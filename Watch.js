class Watch{
    constructor(merk,warna,waterResistant){
        this.merk = merk;
        this.warna = warna;
        this.waterResistant = waterResistant;
    }

    time(){
        let date = new Date().toLocaleString()
        return date;
    }

}

let babyg = new Watch("Baby-G","Soft green","yes");

console.log(babyg.time()); 